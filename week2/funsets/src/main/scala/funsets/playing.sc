package week2

object playing {
  
  def product(f: Int => Int): (Int, Int) => Int = {
    def prodF(a: Int, b: Int): Int =
      if (a > b) 1
      else f(a)*prodF(a + 1, b)
    prodF
  }                                               //> product: (f: Int => Int)(Int, Int) => Int
  
  def fact(n: Int) = product(x => x)(1, n)        //> fact: (n: Int)Int
  fact(5)                                         //> res0: Int = 120
  
  def prodCube = product(x => x*x*x)              //> prodCube: => (Int, Int) => Int
  prodCube(3,4)                                   //> res1: Int = 1728
  product(x => x*x*x)(3,4)                        //> res2: Int = 1728
  
  
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int): (Int, Int) => Int = {
    def tailMapReduce(a: Int, b: Int): Int = {
      if (a > b) zero
      else combine(f(a), tailMapReduce(a + 1, b))
    }
    tailMapReduce
  }                                               //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, zero: Int)(Int, Int) 
                                                  //| => Int
  
  def productMR(f: Int => Int): (Int, Int) => Int = mapReduce(f, (x, y) => x*y, 1)
                                                  //> productMR: (f: Int => Int)(Int, Int) => Int
  def prodCubeMR = productMR(x => x*x*x)          //> prodCubeMR: => (Int, Int) => Int
  prodCubeMR(3, 4)                                //> res3: Int = 1728
}