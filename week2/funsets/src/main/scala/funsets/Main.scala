package funsets

object Main extends App {
    import FunSets._
    println(contains(singletonSet(1), 1))


    val a = union(singletonSet(2), singletonSet(3))
    val b = union(a, singletonSet(4))
    printSet(b)

    println("farall: " + forall(b, x => x < 5))
}
