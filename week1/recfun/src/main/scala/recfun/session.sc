package week1

object session {
  1 + 2                                           //> res0: Int(3) = 3
  def abs(x: Double): Double = {
  	if (x < 0) -x
  	else x
  }                                               //> abs: (x: Double)Double
  
  abs(-100)                                       //> res1: Double = 100.0
  
  
  def factorial(n : Int) : Int = {
    def loop(acc: Int, n: Int) : Int =
      if (n==0) acc
      else loop(acc*n, n-1)
    loop(1,n)
  }                                               //> factorial: (n: Int)Int
  
  factorial(3)                                    //> res2: Int = 6
  
  
  
  def sum(f: Int => Int, a: Int, b: Int) : Int = {
    if(a > b) 0
    else sum(f, a+1, b) + f(a)
  }                                               //> sum: (f: Int => Int, a: Int, b: Int)Int
  
  sum(x => x, 3, 5)                               //> res3: Int = 12
  
  sum(x => x*x, 3,4)                              //> res4: Int = 25
  
  def sumInts(a: Int, b: Int) = sum(x => x, a, b) //> sumInts: (a: Int, b: Int)Int
  
  def sum2(f: Int => Int): (Int, Int) => Int = {
    def sumF(a: Int, b: Int) : Int =
      if(a > b) 0
      else sumF(a+1, b) + f(a)
    sumF
  }                                               //> sum2: (f: Int => Int)(Int, Int) => Int
  
  
  def sumInt = sum2(x => x)                       //> sumInt: => (Int, Int) => Int
  
  
}